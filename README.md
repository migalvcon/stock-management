# Prerequisites
1. Java 8
2. SBT 0.13.15

# Build
The next command builds the project. SBT will produce a ZIP in **target/universal/stock-management-1.0.zip**.

```
sbt universal:packageBin
```

# Run

This zip file will contain two folders: **bin** that will contain the execution scripts and **lib** that contains all of the project JAR dependencies and **Stock Management** JAR.
To run it you have to unpack mentioned ZIP file, navigate to **bin** folder and execute **stock-management** or **stock-management.bat** script (based on operating system).

# Configuration
## application.conf
The application configuration consists in two sections: **applicationDB** and **webServer**.

**applicationDB** contains the connection details to connect to the database. Currently, only SQLite is supported as database. We recommend to keep the configuration as they are in the default **application.conf**,
but the location of the database can be changed by modifying the **applicationDB.url** section to specify the filesystem location of the database (e.g. **jdbc:sqlite:data.sqlite:/opt/db.db**).

```
applicationDB = {
  url = "jdbc:sqlite:data.sqlite"
  driver = org.sqlite.JDBC
  connectionPool = disabled
  keepAliveConnection = true
}
```

**webServer** contains the configuration for the web server. The sections of the configuration are:
* **root**: prefix to the API in the webserver (e.g. **/stock-management** would make the API **foo** accessible through **/stock-management/foo**).
* **port**: port of the web server.

## logback.xml
Allows to define the logging level of the application. This is a standard **logback** configuration.

# API

The project contains a Postman collections with an example of each request. However, they are defined below.

## Initialize

### POST /initialize

Creates the database.

* Return statuses: 204 if the call returns without issues and 500 if an internal error occurred.

## Category API

### GET /categories

Returns the list of all categories present in the application.

* Output format:

```
Array[{
    "categoryId": Long,
    "name": String,
    "description": String
}]
```

* Return statuses: 200 if the call returns without issues and 500 if an internal error occurred.

### POST /categories

Creates new categories in the application.

* Input format:
    * categoryId: it is ignored.
    * name: category name.
    * description: category description.

```
Array[{
    "categoryId": Long,
    "name": String,
    "description": String
}]
```

* Output format:
    * categoryId: it is generated.
    * name: category name.
    * description: category description.

```
Array[{
    "categoryId": Long,
    "name": String,
    "description": String
}]
```
* Return statuses: 200 if the call returns without issues, 500 if an internal error occurred and 400 if the body is incorrectly formatted.

## Client API

### GET /clients

Returns the list of all clients present in the application.

* Output format:

```
Array[{
    "clientId": Long,
    "name": String,
    "surname": String
}]
```

* Return statuses: 200 if the call returns without issues and 500 if an internal error occurred.

### POST /clients

Creates new clients in the application.

* Input format:
    * clientId: it is ignored.
    * name: client name.
    * surname: client surname.

```
Array[{
    "clientId": Long,
    "name": String,
    "surname": String
}]
```

* Output format:
    * clientId: it is generated.
    * name: client name.
    * surname: client description.

```
Array[{
    "clientId": Long,
    "name": String,
    "surname": String
}]
```
* Return statuses: 200 if the call returns without issues, 500 if an internal error occurred and 400 if the body is incorrectly formatted.

## Product API

### GET /products

Returns the list of all products present in the application.

* Output format:

```
Array[{
    "productId": Long,
    "name": String,
    "description": String,
    "category": Category,
    "prize": Double,
    "stock": Int
}]
```

* Return statuses: 200 if the call returns without issues and 500 if an internal error occurred.

### POST /products

Creates new products in the application.

* Input format:
    * productId: it is ignored.
    * name: product name.
    * description: product description.
    * category: category id.
    * prize: product prize.
    * stock: product quantity.

```
Array[{
    "productId": Long,
    "name": String,
    "description": String,
    "category": Long,
    "prize": Double,
    "stock": Int
}]
```

* Output format:
    * productId: it is generated.
    * name: product name.
    * description: product description.

```
Array[{
    "productId": Long,
    "name": String,
    "description": String
}]
```
* Return statuses: 200 if the call returns without issues, 500 if an internal error occurred and 400 if the body is incorrectly formatted.

## Reserve API

### GET /{clientId}/reserves

Returns the list of all reserves of a client present in the application.

* Output format:

```
Array[{
    "reserveId": Long,
    "client": Client,
    "product": {
        "productId": Long,
        "name": String,
        "description": String
    }
    "quantity": Int,
    "timestamp": Long
}]
```

* Return statuses: 200 if the call returns without issues, 500 if an internal error occurred and 400 if the client does not exist.

### POST /{clientId}/reserves

Creates new reserves for a client in the application.

* Input format:
    * productId: product id.
    * quantity: number of products.

```
Array[{
    "productId": Long,
    "quantity": Int
}]
```

* Output format:
    * reserveId: it is generated.
    * client: the client.
    * product: product summary.
    * quantity: number of products reserved (it can not be max than the number of available products).
    * timestamp: reserved date in seconds.

```
Array[{
    "reserveId": Long,
    "client": Client,
    "product": {
        "productId": Long,
        "name": String,
        "description": String
    }
    "quantity": Int,
    "timestamp": Long
}]
```
* Return statuses: 200 if the call returns without issues, 500 if an internal error occurred and 400 if the body is incorrectly formatted or the client does not exist.

### DELETE /{clientId}/reserves/{reserveId}

Deletes a reserve of a client in the application.

* Return statuses: 204 if the call returns without issues, 500 if an internal error occurred and 400 if either the client or the reserve does not exist.

## Purchase API

### GET /{clientId}/purchases

Returns the list of all purchases of a client present in the application.

* Output format:

```
Array[{
    "purchaseId": Long,
    "client": Client,
    "product": {
        "productId": Long,
        "name": String,
        "description": String
    }
    "quantity": Int,
    "timestamp": Long
}]
```

* Return statuses: 200 if the call returns without issues, 500 if an internal error occurred and 400 if the client does not exist.

### POST /{clientId}/purchases

Creates new purchases for a client in the application.

* Input format:
    * productId: product id.
    * quantity: number of products.

```
Array[{
    "productId": Long,
    "quantity": Int
}]
```

* Output format:
    * purchaseId: it is generated.
    * client: the client.
    * product: product summary.
    * quantity: number of products purchased (it can not be max than the number of available products).
    * timestamp: reserved date in seconds.

```
Array[{
    "purchaseId": Long,
    "client": Client,
    "product": {
        "productId": Long,
        "name": String,
        "description": String
    }
    "quantity": Int,
    "timestamp": Long
}]
```
* Return statuses: 200 if the call returns without issues, 500 if an internal error occurred and 400 if the body is incorrectly formatted or the client does not exist.

## Order API

### GET /orders

Returns the list of all orders present in the application.

* Output format:

```
Array[{
    "orderId": Long,
    "client": Client,
    "product": {
        "productId": Long,
        "name": String,
        "description": String
    }
    "quantity": Int,
    "timestamp": Long
}]
```

* Return statuses: 200 if the call returns without issues and 500 if an internal error occurred.

### GET /{productId}/orders

Returns the list of all orders of a product present in the application.

* Output format:

```
Array[{
    "orderId": Long,
    "client": Client,
    "product": {
        "productId": Long,
        "name": String,
        "description": String
    }
    "quantity": Int,
    "timestamp": Long
}]
```

* Return statuses: 200 if the call returns without issues, 500 if an internal error occurred and 400 if the product does not exist.

### POST /orders

Creates new orders in the application.

* Input format:
    * productId: product id.
    * quantity: number of products.

```
Array[{
    "productId": Long,
    "quantity": Int
}]
```

* Output format:
    * orderId: it is generated.
    * product: product summary.
    * quantity: number of products purchased (it can not be max than the number of available products).
    * timestamp: reserved date in seconds.

```
Array[{
    "purchaseId": Long,
    "product": {
        "productId": Long,
        "name": String,
        "description": String
    }
    "quantity": Int,
    "timestamp": Long
}]
```
* Return statuses: 200 if the call returns without issues, 500 if an internal error occurred and 400 if the body is incorrectly formatted.

# Usage guides

## Set up step

1. Prepare **application.conf** and **logback.xml**.
2. Build the project.
3. Start the application.
4. Initialize the database with the request **POST /initialize**.
5. Add some clients with the request **POST /clients**.
6. Add some categories with the request **POST /categories**.
7. Add some products with the request **POST /products**.