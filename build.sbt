scalaVersion := "2.12.2"

name := "stock-management"
version := "1.0"

val http4sVersion = "0.15.4a"
val circeVersion = "0.7.1"
val specs2Version = "3.8.9"

libraryDependencies ++= Seq(
  "io.verizon.journal"  %%  "core"                  % "3.0.18",
  "net.jcazevedo"       %%  "moultingyaml"          % "0.4.0",
  "org.rogach"          %%  "scallop"               % "2.1.0",
  "org.http4s"          %%  "http4s-blaze-server"   % http4sVersion,
  "org.http4s"          %%  "http4s-blaze-client"   % http4sVersion,
  "org.http4s"          %%  "http4s-dsl"            % http4sVersion,
  "org.http4s"          %%  "http4s-circe"          % http4sVersion,
  "io.circe"            %%  "circe-generic"         % circeVersion,
  "io.circe"            %%  "circe-literal"         % circeVersion,
  "org.specs2"          %%  "specs2-core"           % specs2Version % "test,it",
  "org.specs2"          %%  "specs2-scalaz"         % specs2Version % "test,it",
  "org.specs2"          %%  "specs2-matcher-extra"  % specs2Version % "test,it",
  "com.typesafe.slick"  %%  "slick"                 % "3.2.0",
  "org.xerial"          %   "sqlite-jdbc"           % "3.16.1"
)

lazy val root = (project in file(".")).
  configs(IntegrationTest).
  settings(Defaults.itSettings: _*)

testOptions in Test += Tests.Argument("showtimes")
testOptions in IntegrationTest += Tests.Argument("showtimes")

enablePlugins(JavaServerAppPackaging)