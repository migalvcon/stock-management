package me.maalvarez.database.dao

import me.maalvarez.database.ClientSchema.{clients => cls}
import me.maalvarez.database.ProductSchema.{products => prs}
import me.maalvarez.database.ReserveSchema.{reserves => rs}
import me.maalvarez.model.Out.{Product, ProductSummary, Reserve}
import me.maalvarez.model.Client
import slick.jdbc.SQLiteProfile.api._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

class ReserveDao(database: Database, executionContext: ExecutionContext = ExecutionContext.global) {
  private implicit val localExecutionContext = executionContext

  def getByClient(id: Long): List[Reserve] = {
    val query = for {
      r <- rs
      c <- cls if r.clientId === c.clientId && c.clientId === id
      p <- prs if r.productId === p.productId
    } yield (r, c, p)

    Await.result({
      for {
        joinSeq <- database.run(query.result)
      } yield joinSeq.toList.map(row => {
        val (r, cl, p) = row

        val client = Client(cl._1, cl._2, cl._3)
        val productSummary = ProductSummary(p._1, p._2, p._3)

        Reserve(r._1, client, productSummary, r._4, r._5)
      })
    }, Duration.Inf)
  }

  def getSubsetByClient(cId: Long, rIds: List[Long]): List[Reserve] = {
    val query = for {
      r <- rs if r.reserveId inSet rIds
      c <- cls if r.clientId === c.clientId && c.clientId === cId
      p <- prs if r.productId === p.productId
    } yield (r, c, p)

    Await.result({
      for {
        joinSeq <- database.run(query.result)
      } yield joinSeq.toList.map(row => {
        val (r, cl, p) = row

        val client = Client(cl._1, cl._2, cl._3)
        val productSummary = ProductSummary(p._1, p._2, p._3)

        Reserve(r._1, client, productSummary, r._4, r._5)
      })
    }, Duration.Inf)
  }

  def add(c: Client, p: Product, q: Int, t: Long): Reserve = {
    val query = rs returning rs.map(_.reserveId) into ((r, id) => r.copy(id))

    val action = query += (0, c.clientId, p.productId, q, t)

    Await.result({
      for {
        r <- database.run(action)
      } yield Reserve(r._1, c, ProductSummary(p.productId, p.name, p.description), q, t)
    }, Duration.Inf)
  }

  def delete(id: Long): Unit = {
    val query = rs.filter(_.reserveId === id)
    val action = query.delete

    Await.result({
      database.run(action)
    }, Duration.Inf)
  }
}
