package me.maalvarez.database.dao

import me.maalvarez.database.CategorySchema.{categories => cs}
import me.maalvarez.model.Category
import slick.jdbc.SQLiteProfile.api._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

class CategoryDao(database: Database, executionContext: ExecutionContext = ExecutionContext.global) {
  private implicit val localExecutionContext = executionContext

  def getAll(): List[Category] = {
    Await.result({
      for {
        categoriesSeq <- database.run(cs.result)
      } yield for {
        category <- categoriesSeq.toList
        (id, name, description) = category
      } yield Category(id, name, description)
    }, Duration.Inf)
  }

  def addMany(c: List[Category]): List[Category] = c.map(add(_))

  private def add(c: Category): Category = {
    val query = cs returning cs.map(_.categoryId) into ((c, id) => c.copy(id))

    val action = query += (0, c.name, c.description)

    Await.result({
      for {
        c <- database.run(action)
      } yield Category(c._1, c._2, c._3)
    }, Duration.Inf)
  }
}
