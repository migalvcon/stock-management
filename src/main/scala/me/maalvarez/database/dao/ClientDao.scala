package me.maalvarez.database.dao

import me.maalvarez.database.ClientSchema.{clients => cs}
import me.maalvarez.model.Client
import slick.jdbc.SQLiteProfile.api._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

class ClientDao(database: Database, executionContext: ExecutionContext = ExecutionContext.global) {
  private implicit val localExecutionContext = executionContext

  def getAll(): List[Client] = {
    Await.result({
      for {
        clientsSeq <- database.run(cs.result)
      } yield for {
        client <- clientsSeq.toList
        (id, name, surname) = client
      } yield Client(id, name, surname)
    }, Duration.Inf)
  }

  def getById(id: Long): Option[Client] = {
    val query = for {
      client <- cs if client.clientId === id
    } yield client

    Await.result({
      for {
        clientsSeq <- database.run(query.result)
      } yield clientsSeq.headOption.map(c => Client(c._1, c._2, c._3))
    }, Duration.Inf)
  }

  def addMany(c: List[Client]): List[Client] = c.map(add(_))

  private def add(c: Client): Client = {
    val query = cs returning cs.map(_.clientId) into ((c, id) => c.copy(id))

    val action = query += (0, c.name, c.surname)

    Await.result({
      for {
        c <- database.run(action)
      } yield Client(c._1, c._2, c._3)
    }, Duration.Inf)
  }
}
