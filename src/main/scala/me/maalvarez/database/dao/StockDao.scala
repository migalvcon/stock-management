package me.maalvarez.database.dao

import me.maalvarez.database.ClientSchema.{clients => cls}
import me.maalvarez.database.ProductSchema.{products => prs}
import me.maalvarez.database.PurchaseSchema.{purchases => pus}
import me.maalvarez.database.OrderSchema.{orders => os}
import me.maalvarez.model.Out.{Order, Product, ProductSummary, Purchase}
import me.maalvarez.model.Client
import slick.jdbc.SQLiteProfile.api._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

class StockDao(database: Database, executionContext: ExecutionContext = ExecutionContext.global) {
  private implicit val localExecutionContext = executionContext

  def getPurchasesByClient(id: Long): List[Purchase] = {
    val innerJoin = for {
      pu <- pus
      c <- cls if pu.clientId === c.clientId && c.clientId === id
      pr <- prs if pu.productId === pr.productId
    } yield (pu, c, pr)

    Await.result({
      for {
        joinSeq <- database.run(innerJoin.result)
      } yield joinSeq.toList.map(row => {
        val (pu, c, pr) = row

        val client = Client(c._1, c._2, c._3)
        val productSummary = ProductSummary(pr._1, pr._2, pr._3)

        Purchase(pu._1, client, productSummary, pu._4, pu._5)
      })
    }, Duration.Inf)
  }

  def getPurchaseSubset(ids: List[Long]): List[Purchase] = {
    val innerJoin = for {
      pu <- pus if pu.purchaseId inSet ids
      c <- cls if pu.clientId === c.clientId
      pr <- prs if pu.productId === pr.productId
    } yield (pu, c, pr)

    Await.result({
      for {
        joinSeq <- database.run(innerJoin.result)
      } yield joinSeq.toList.map(row => {
        val (pu, c, pr) = row

        val client = Client(c._1, c._2, c._3)
        val productSummary = ProductSummary(pr._1, pr._2, pr._3)

        Purchase(pu._1, client, productSummary, pu._4, pu._5)
      })
    }, Duration.Inf)
  }

  def addPurchase(c: Client, p: Product, q: Int, t: Long): Purchase = {
    val query = pus returning pus.map(_.purchaseId) into ((p, id) => p.copy(id))

    val action = query += (0, c.clientId, p.productId, q, t)

    Await.result({
      for {
        pu <- database.run(action)
      } yield Purchase(pu._1, c, ProductSummary(p.productId, p.name, p.description), pu._4, pu._5)
    }, Duration.Inf)
  }

  def getAllOrders(): List[Order] = {
    val query = for {
      o <- os
      p <- prs if o.productId === p.productId
    } yield (o, p)

    Await.result({
      for {
        joinSeq <- database.run(query.result)
      } yield joinSeq.toList.map(row => {
        val (o, p) = row

        val productSummary = ProductSummary(p._1, p._2, p._3)

        Order(o._1, productSummary, o._3, o._4)
      })
    }, Duration.Inf)
  }

  def getOrdersByProduct(id: Long): List[Order] = {
    val query = for {
      o <- os
      p <- prs if o.productId === p.productId && p.productId === id
    } yield (o, p)

    Await.result({
      for {
        joinSeq <- database.run(query.result)
      } yield joinSeq.toList.map(row => {
        val (o, p) = row

        val productSummary = ProductSummary(p._1, p._2, p._3)

        Order(o._1, productSummary, o._3, o._4)
      })
    }, Duration.Inf)
  }

  def getOrderSubset(ids: List[Long]): List[Order] = {
    val query = for {
      o <- os if o.orderId inSet ids
      p <- prs if o.productId === p.productId
    } yield (o, p)

    Await.result({
      for {
        joinSeq <- database.run(query.result)
      } yield joinSeq.toList.map(row => {
        val (o, p) = row

        val productSummary = ProductSummary(p._1, p._2, p._3)

        Order(o._1, productSummary, o._3, o._4)
      })
    }, Duration.Inf)
  }

  def addOrder(p: Product, q: Int, t: Long): Order = {
    val query = os returning os.map(_.orderId) into ((o, id) => o.copy(id))

    val action = query += (0, p.productId, q, t)

    Await.result({
      for {
        o <- database.run(action)
      } yield Order(o._1, ProductSummary(p.productId, p.name, p.description), o._3, o._4)
    }, Duration.Inf)
  }
}