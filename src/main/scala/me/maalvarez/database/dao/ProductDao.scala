package me.maalvarez.database.dao

import me.maalvarez.database.ProductSchema.{products => ps}
import me.maalvarez.database.CategorySchema.{categories => cs}
import me.maalvarez.model.Category
import me.maalvarez.model.In.{Product => ProductIn}
import me.maalvarez.model.Out.{ProductSummary, Product => ProductOut}
import slick.jdbc.SQLiteProfile.api._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

class ProductDao(database: Database, executionContext: ExecutionContext = ExecutionContext.global) {
  private implicit val localExecutionContext = executionContext

  def getAll(): List[ProductOut] = {
    val query = for {
      p <- ps
      c <- cs if p.categoryId === c.categoryId
    } yield (p, c)

    Await.result({
      for {
        joinSeq <- database.run(query.result)
      } yield joinSeq.toList.map(row => {
        val (p, c) = row

        val category = Category(c._1, c._2, c._3)

        ProductOut(p._1, p._2, p._3, category, p._5, p._6)
      })
    }, Duration.Inf)
  }

  def getById(id: Long): Option[ProductOut] = {
    val query = for {
      p <- ps
      c <- cs if p.categoryId === c.categoryId
    } yield (p, c)

    Await.result({
      for {
        joinSeq <- database.run(query.result)
      } yield joinSeq.headOption.map(row => {
        val (p, c) = row

        val category = Category(c._1, c._2, c._3)

        ProductOut(p._1, p._2, p._3, category, p._5, p._6)
      })
    }, Duration.Inf)
  }

  def addMany(p: List[ProductIn]): List[ProductSummary] = p.map(add(_))

  private def add(p: ProductIn): ProductSummary = {
    val query = ps returning ps.map(_.productId) into ((p, id) => p.copy(id))

    val action = query += (0, p.name, p.description, p.categoryId, p.prize, p.stock)

    Await.result({
      for {
        p <- database.run(action)
      } yield ProductSummary(p._1, p._2, p._3)
    }, Duration.Inf)
  }

  def update(id: Long, q: Int): Unit = {
    val query = for {
      product <- ps if product.productId === id
    } yield product.stock

    val action = query.update(q)

    Await.result({
      database.run(action)
    }, Duration.Inf)
  }
}
