package me.maalvarez.database

import slick.jdbc.SQLiteProfile.api._

final class CategorySchema(tag: Tag) extends Table[(Long, String, String)](tag, "CATEGORY") {
  def categoryId = column[Long]("CATEGORY_ID", O.PrimaryKey, O.AutoInc)

  def name = column[String]("NAME")

  def description = column[String]("DESCRIPTION")

  def * = (categoryId, name, description)
}

object CategorySchema {
  val categories = TableQuery[CategorySchema]
}

final class ClientSchema(tag: Tag) extends Table[(Long, String, String)](tag, "CLIENT") {
  def clientId = column[Long]("CLIENT_ID", O.PrimaryKey, O.AutoInc)

  def name = column[String]("NAME")

  def surname = column[String]("SURNAME")

  def * = (clientId, name, surname)
}

object ClientSchema {
  val clients = TableQuery[ClientSchema]
}

final class OrderSchema(tag: Tag) extends Table[(Long, Long, Int, Long)](tag, "ORDER") {
  def orderId = column[Long]("ORDER_ID", O.PrimaryKey, O.AutoInc)

  def productId = column[Long]("PRODUCT_ID")

  def quantity = column[Int]("QUANTITY")

  def timestamp = column[Long]("TIMESTAMP")

  def * = (orderId, productId, quantity, timestamp)

  def product = foreignKey("ORDER_PRODUCT_FK", productId, ProductSchema.products)(_.productId)
}

object OrderSchema {
  val orders = TableQuery[OrderSchema]
}

final class ProductSchema(tag: Tag) extends Table[(Long, String, String, Long, Double, Int)](tag, "PRODUCT") {
  def productId = column[Long]("PRODUCT_ID", O.PrimaryKey, O.AutoInc)

  def name = column[String]("NAME")

  def description = column[String]("DESCRIPTION")

  def categoryId = column[Long]("CATEGORY_ID")

  def prize = column[Double]("PRIZE")

  def stock = column[Int]("STOCK")

  def * = (productId, name, description, categoryId, prize, stock)

  def category = foreignKey("PRODUCT_CATEGORY_FK", categoryId, CategorySchema.categories)(_.categoryId)
}

object ProductSchema {
  val products = TableQuery[ProductSchema]
}

final class PurchaseSchema(tag: Tag) extends Table[(Long, Long, Long, Int, Long)](tag, "PURCHASE") {
  def purchaseId = column[Long]("PURCHASE_ID", O.PrimaryKey, O.AutoInc)

  def clientId = column[Long]("CLIENT_ID")

  def productId = column[Long]("PRODUCT_ID")

  def quantity = column[Int]("QUANTITY")

  def timestamp = column[Long]("TIMESTAMP")

  def * = (purchaseId, clientId, productId, quantity, timestamp)

  def client = foreignKey("PURCHASE_CLIENT_FK", clientId, ClientSchema.clients)(_.clientId)

  def product = foreignKey("PURCHASE_PRODUCT_FK", productId, ProductSchema.products)(_.productId)
}

object PurchaseSchema {
  val purchases = TableQuery[PurchaseSchema]
}

final class ReserveSchema(tag: Tag) extends Table[(Long, Long, Long, Int, Long)](tag, "RESERVE") {
  def reserveId = column[Long]("RESERVE_ID", O.PrimaryKey, O.AutoInc)

  def clientId = column[Long]("CLIENT_ID")

  def productId = column[Long]("PRODUCT_ID")

  def quantity = column[Int]("QUANTITY")

  def timestamp = column[Long]("TIMESTAMP")

  def * = (reserveId, clientId, productId, quantity, timestamp)

  def client = foreignKey("RESERVE_CLIENT_FK", clientId, ClientSchema.clients)(_.clientId)

  def product = foreignKey("RESERVE_PRODUCT_FK", productId, ProductSchema.products)(_.productId)
}

object ReserveSchema {
  val reserves = TableQuery[ReserveSchema]
}