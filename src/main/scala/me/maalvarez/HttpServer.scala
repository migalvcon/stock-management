package me.maalvarez

import com.typesafe.config.ConfigFactory
import journal.Logger
import me.maalvarez.endpoints._
import org.http4s.server.blaze.BlazeBuilder
import org.http4s.server.{Server, ServerApp}

import scalaz.concurrent.Task

object HttpServer extends ServerApp {
  private val logger = Logger[this.type]

  private val WebServerNameKey = "webServer.name"
  private val WebServerPortKey = "webServer.port"

  override def server(args: List[String]): Task[Server] = {
    val config = ConfigFactory.load()

    val name = config.getString(WebServerNameKey)
    val port = config.getInt(WebServerPortKey)

    logger.info(s"Starting server: ${name}:${port}...")

    BlazeBuilder
      .bindHttp(port, name)
      .mountService(Categories.service, "/")
      .mountService(Clients.service, "/")
      .mountService(Initialize.service, "/")
      .mountService(Products.service, "/")
      .mountService(Reserves.service, "/")
      .mountService(Stock.service, "/")
      .start
  }
}
