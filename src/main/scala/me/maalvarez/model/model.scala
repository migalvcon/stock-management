package me.maalvarez.model

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

final case class Category(categoryId: Long, name: String, description: String)

object Category {
  implicit val categoryEncoder: Encoder[Category] = deriveEncoder
  implicit val categoryDecoder: Decoder[Category] = deriveDecoder
}

final case class Client(clientId: Long, name: String, surname: String)

object Client {
  implicit val clientEncoder: Encoder[Client] = deriveEncoder
  implicit val clientDecoder: Decoder[Client] = deriveDecoder
}

object In {
  final case class Order(productId: Long, quantity: Int)

  object Order {
    implicit val orderEncoder: Encoder[In.Order] = deriveEncoder
    implicit val orderDecoder: Decoder[In.Order] = deriveDecoder
  }

  final case class Product(productId: Long, name: String, description: String, categoryId: Long, prize: Double, stock: Int)

  object Product {
    implicit val productEncoder: Encoder[In.Product] = deriveEncoder
    implicit val productDecoder: Decoder[In.Product] = deriveDecoder
  }

  final case class Purchase(productId: Long, quantity: Int)

  object Purchase {
    implicit val reserveEncoder: Encoder[In.Purchase] = deriveEncoder
    implicit val reserveDecoder: Decoder[In.Purchase] = deriveDecoder
  }

  final case class Reserve(productId: Long, quantity: Int)

  object Reserve {
    implicit val reserveEncoder: Encoder[In.Reserve] = deriveEncoder
    implicit val reserveDecoder: Decoder[In.Reserve] = deriveDecoder
  }
}

object Out {
  final case class Order(orderId: Long, product: ProductSummary, quantity: Int, timestamp: Long)

  object Order {
    implicit val orderEncoder: Encoder[Out.Order] = deriveEncoder
    implicit val orderDecoder: Decoder[Out.Order] = deriveDecoder
  }

  final case class Product(productId: Long, name: String, description: String, category: Category, prize: Double, stock: Int)

  object Product {
    implicit val productEncoder: Encoder[Out.Product] = deriveEncoder
    implicit val productDecoder: Decoder[Out.Product] = deriveDecoder
  }

  final case class ProductSummary(productId: Long, name: String, description: String)

  object ProductSummary {
    implicit val productEncoder: Encoder[ProductSummary] = deriveEncoder
    implicit val productDecoder: Decoder[ProductSummary] = deriveDecoder
  }

  final case class Purchase(purchaseId: Long, client: Client, product: ProductSummary, quantity: Int, timestamp: Long)

  object Purchase {
    implicit val purchaseEncoder: Encoder[Out.Purchase] = deriveEncoder
    implicit val purchaseDecoder: Decoder[Out.Purchase] = deriveDecoder
  }

  final case class Reserve(reserveId: Long, client: Client, product: ProductSummary, quantity: Int, timestamp: Long)

  object Reserve {
    implicit val reserveEncoder: Encoder[Out.Reserve] = deriveEncoder
    implicit val reserveDecoder: Decoder[Out.Reserve] = deriveDecoder
  }
}