package me.maalvarez.util

object Messages {
  val InvalidJsonMessage = "Invalid JSON object"

  val InitializationSuccessMessage = "Database created"
  val InitializationWarningMessage = "The me.maalvarez.database is already created"
  val InitializationErrorMessage = "Error while initialising the me.maalvarez.database"

  val RequestSuccessMessage = "Success operation"
  val RequestErrorMessage = "Error while retrieving information"

  def clientNotFoundMessage(clientId: Long) = s"The client does not exist - ${clientId}"
}
