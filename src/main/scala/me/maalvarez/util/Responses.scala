package me.maalvarez.util

import io.circe.literal._
import org.http4s.circe._
import org.http4s.dsl._

object Responses {
  def systemErrorResponse(message: String) =
    InternalServerError(json"""{"systemMessage": ${message}}""")

  def userErrorResponse(message: String) =
    BadRequest(json"""{"userMessage": ${message}}""")

  def clientNotFoundErrorResponse(clientId: Long) =
    userErrorResponse(Messages.clientNotFoundMessage(clientId))
}
