package me.maalvarez.endpoints

import me.maalvarez.database.dao.ClientDao
import me.maalvarez.endpoints.Categories.database
import io.circe.syntax._
import journal.Logger
import me.maalvarez.model.Client
import org.http4s.circe._
import org.http4s.dsl._
import org.http4s.{DecodeFailure, HttpService}
import me.maalvarez.util._

import scala.util.control.NonFatal

object Clients extends AbstractEndpoint {
  private val logger = Logger[this.type]

  val service = HttpService {
    case GET -> Root / "clients" => {
      logger.info(s"GET /clients")

      try {
        val clientDao = new ClientDao(database)
        val clients = clientDao.getAll()

        Ok(clients.asJson)
      } catch {
        case e: Exception => {
          logger.error(Messages.RequestErrorMessage, e)

          Responses.systemErrorResponse(Messages.RequestErrorMessage)
        }
      }
    }

    case request @ POST -> Root / "clients" => {
      logger.info(s"POST /clients")

      val clientsRequest = request.as(jsonOf[List[Client]])
      clientsRequest.flatMap(clients => {
        val clientDao = new ClientDao(database)
        val insertedClients = clientDao.addMany(clients)

        Ok(insertedClients.asJson)
      }).handleWith {
        case e: DecodeFailure => {
          logger.error(s"Invalid JSON object: ${request.body}", e)

          Responses.userErrorResponse(Messages.InvalidJsonMessage)
        }
        case NonFatal(e) => {
          logger.error(s"Unexpected error", e)

          Responses.systemErrorResponse(Messages.RequestErrorMessage)
        }
      }
    }
  }
}
