package me.maalvarez.endpoints

import java.time.Instant

import me.maalvarez.database.dao.{ClientDao, ProductDao, ReserveDao}
import io.circe.syntax._
import journal.Logger
import me.maalvarez.model.In.{Reserve => ReserveIn}
import me.maalvarez.model.Out.{Reserve => ReserveOut}
import org.http4s.circe._
import org.http4s.dsl._
import org.http4s.{DecodeFailure, HttpService}
import me.maalvarez.util._

import scala.collection.mutable.ListBuffer
import scala.util.control.NonFatal

object Reserves extends AbstractEndpoint {
  private val logger = Logger[this.type]

  val service = HttpService {
    case GET -> Root / LongVar(clientId) / "reserves" => {
      logger.info(s"GET /${clientId}/reserves")

      try {
        val clientDao = new ClientDao(database)
        clientDao.getById(clientId) match {
          case None => {
            logger.error(Messages.clientNotFoundMessage(clientId))

            Responses.clientNotFoundErrorResponse(clientId)
          }
          case Some(_) => {
            val reserveDao = new ReserveDao(database)
            val reserves = reserveDao.getByClient(clientId)

            Ok(reserves.asJson)
          }
        }
      } catch {
        case e: Exception => {
          logger.error(Messages.RequestErrorMessage, e)

          Responses.systemErrorResponse(Messages.RequestErrorMessage)
        }
      }
    }

    case request@POST -> Root / LongVar(clientId) / "reserves" => {
      logger.info(s"POST /${clientId}/reserve")

      try {
        val clientDao = new ClientDao(database)
        clientDao.getById(clientId) match {
          case None => {
            logger.error(Messages.clientNotFoundMessage(clientId))

            Responses.clientNotFoundErrorResponse(clientId)
          }
          case Some(c) => {
            val reservesRequest = request.as(jsonOf[List[ReserveIn]])

            reservesRequest.flatMap(reserves => {
              val timestamp = Instant.now().getEpochSecond()
              val reserveDao = new ReserveDao(database)
              val productDao = new ProductDao(database)
              val confirmedReserves: ListBuffer[ReserveOut] = new ListBuffer[ReserveOut]()

              for (reserve <- reserves) {
                productDao.getById(reserve.productId) match {
                  case Some(p) => {
                    val quantity = Math.min(p.stock, reserve.quantity)

                    confirmedReserves += reserveDao.add(c, p, quantity, timestamp)

                    productDao.update(reserve.productId, p.stock - quantity)
                  }
                }
              }

              Ok(confirmedReserves.asJson)
            }).handleWith {
              case e: DecodeFailure => {
                logger.error(s"Invalid JSON object: ${request.body}", e)

                Responses.systemErrorResponse(Messages.RequestErrorMessage)
              }
            }
          }
        }
      } catch {
        case NonFatal(e) => {
          logger.error(s"Unexpected error", e)

          Responses.systemErrorResponse(Messages.RequestErrorMessage)
        }
      }
    }

    case request@DELETE -> Root / LongVar(clientId) / "reserve" / LongVar(reserveId) => {
      logger.info(s"DELETE /${clientId}/reserve/${reserveId}")

      try {
        val clientDao = new ClientDao(database)
        clientDao.getById(clientId) match {
          case None => {
            logger.error(Messages.clientNotFoundMessage(clientId))

            Responses.clientNotFoundErrorResponse(clientId)
          }
          case Some(_) => {
            val reserveDao = new ReserveDao(database)

            reserveDao.getSubsetByClient(clientId, List(reserveId)) match {
              case List(reserve) => {
                val productId = reserve.product.productId

                val productDao = new ProductDao(database)
                val product = productDao.getById(productId).get
                productDao.update(productId, product.stock + reserve.quantity)

                reserveDao.delete(reserveId)
              }
              case Nil => Unit
            }

            NoContent()
          }
        }
      } catch {
        case NonFatal(e) => {
          logger.error(s"Unexpected error", e)

          Responses.systemErrorResponse(Messages.RequestErrorMessage)
        }
      }
    }
  }
}
