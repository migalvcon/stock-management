package me.maalvarez.endpoints

import slick.jdbc.SQLiteProfile.api.Database

abstract class AbstractEndpoint {
  var database = Database.forConfig("applicationDB")
}
