package me.maalvarez.endpoints

import me.maalvarez.database.dao.ProductDao
import me.maalvarez.endpoints.Clients.database
import io.circe.syntax._
import journal.Logger
import me.maalvarez.model.In.Product
import org.http4s.circe._
import org.http4s.dsl._
import org.http4s.{DecodeFailure, HttpService}
import me.maalvarez.util._

import scala.util.control.NonFatal

object Products extends AbstractEndpoint {
  private val logger = Logger[this.type]

  val service = HttpService {
    case GET -> Root / "products" => {
      logger.info(s"GET /products")

      try {
        val productDao = new ProductDao(database)
        val products = productDao.getAll()

        Ok(products.asJson)
      } catch {
        case e: Exception => {
          logger.error(Messages.RequestErrorMessage, e)

          Responses.systemErrorResponse(Messages.RequestErrorMessage)
        }
      }
    }

    case request @ POST -> Root / "products" => {
      logger.info(s"POST /products")

      val productsRequest = request.as(jsonOf[List[Product]])
      productsRequest.flatMap(products => {
        val productDao = new ProductDao(database)
        val insertedProducts = productDao.addMany(products)

        Ok(insertedProducts.asJson)
      }).handleWith {
        case e: DecodeFailure => {
          logger.error(s"Invalid JSON object: ${request.body}", e)

          Responses.userErrorResponse(Messages.InvalidJsonMessage)
        }
        case NonFatal(e) => {
          logger.error(s"Unexpected error", e)

          Responses.systemErrorResponse(Messages.RequestErrorMessage)
        }
      }
    }
  }
}
