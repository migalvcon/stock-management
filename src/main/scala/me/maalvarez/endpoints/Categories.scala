package me.maalvarez.endpoints

import me.maalvarez.database.dao.CategoryDao
import io.circe.syntax._
import journal.Logger
import me.maalvarez.model.Category
import org.http4s.{DecodeFailure, HttpService}
import org.http4s.circe._
import org.http4s.dsl._
import me.maalvarez.util._

import scala.util.control.NonFatal

object Categories extends AbstractEndpoint {
  private val logger = Logger[this.type]

  val service = HttpService {
    case GET -> Root / "categories" => {
      logger.info(s"GET /categories")

      try {
        val categoryDao = new CategoryDao(database)
        val categories = categoryDao.getAll()

        Ok(categories.asJson)
      } catch {
        case e: Exception => {
          logger.error(Messages.RequestErrorMessage, e)

          Responses.systemErrorResponse(Messages.RequestErrorMessage)
        }
      }
    }

    case request @ POST -> Root / "categories" => {
      logger.info(s"POST /categories")

      val categoriesRequest = request.as(jsonOf[List[Category]])
      categoriesRequest.flatMap(categories => {
        val categoryDao = new CategoryDao(database)
        val insertedCategories = categoryDao.addMany(categories)

        Ok(insertedCategories.asJson)
      }).handleWith {
        case e: DecodeFailure => {
          logger.error(s"Invalid JSON object: ${request.body}", e)

          Responses.userErrorResponse(Messages.InvalidJsonMessage)
        }
        case NonFatal(e) => {
          logger.error(s"Unexpected error", e)

          Responses.systemErrorResponse(Messages.RequestErrorMessage)
        }
      }
    }
  }
}
