package me.maalvarez.endpoints

import java.time.Instant

import me.maalvarez.database.dao.{ClientDao, ProductDao, StockDao}
import io.circe.syntax._
import journal.Logger
import me.maalvarez.model.In.{Order => OrderIn, Purchase => PurchaseIn}
import me.maalvarez.model.Out.{Order => OrderOut, Purchase => PurchaseOut}
import org.http4s.circe._
import org.http4s.dsl._
import org.http4s.{DecodeFailure, HttpService}
import me.maalvarez.util._

import scala.collection.mutable.ListBuffer
import scala.util.control.NonFatal

object Stock extends AbstractEndpoint {
  private val logger = Logger[this.type]

  val service = HttpService {
    case GET -> Root / LongVar(clientId) / "purchases" => {
      logger.info(s"GET /${clientId}/purchases")

      try {
        val clientDao = new ClientDao(database)
        clientDao.getById(clientId) match {
          case None => {
            logger.error(Messages.clientNotFoundMessage(clientId))

            Responses.clientNotFoundErrorResponse(clientId)
          }
          case Some(c) => {
            val stockDao = new StockDao(database)
            val purchases = stockDao.getPurchasesByClient(clientId)

            Ok(purchases.asJson)
          }
        }
      } catch {
        case e: Exception => {
          logger.error(Messages.RequestErrorMessage, e)

          Responses.systemErrorResponse(Messages.RequestErrorMessage)
        }
      }
    }

    case request @ POST -> Root / LongVar(clientId) / "purchases" => {
      logger.info(s"POST /${clientId}/purchase")

      try {
        val clientDao = new ClientDao(database)
        clientDao.getById(clientId) match {
          case None => {
            logger.error(Messages.clientNotFoundMessage(clientId))

            Responses.clientNotFoundErrorResponse(clientId)
          }
          case Some(c) => {
            val reservesRequest = request.as(jsonOf[List[PurchaseIn]])

            reservesRequest.flatMap(purchases => {
              val timestamp = Instant.now().getEpochSecond()
              val stockDao = new StockDao(database)
              val productDao = new ProductDao(database)
              val confirmedPurchases: ListBuffer[PurchaseOut] = new ListBuffer[PurchaseOut]()

              for (purchase <- purchases) {
                productDao.getById(purchase.productId) match {
                  case Some(p) => {
                    val quantity = Math.min(p.stock, purchase.quantity)

                    confirmedPurchases += stockDao.addPurchase(c, p, quantity, timestamp)

                    productDao.update(purchase.productId, p.stock - quantity)
                  }
                }
              }

              Ok(confirmedPurchases.asJson)
            }).handleWith {
              case e: DecodeFailure => {
                logger.error(s"Invalid JSON object: ${request.body}", e)

                Responses.userErrorResponse(Messages.InvalidJsonMessage)
              }
            }
          }
        }
      } catch {
        case NonFatal(e) => {
          logger.error(s"Unexpected error", e)

          Responses.systemErrorResponse(Messages.RequestErrorMessage)
        }
      }
    }

    case GET -> Root / "orders" => {
      logger.info(s"GET /orders")

      try {
        val stockDao = new StockDao(database)
        val orders = stockDao.getAllOrders()

        Ok(orders.asJson)
      } catch {
        case e: Exception => {
          logger.error(Messages.RequestErrorMessage, e)

          Responses.systemErrorResponse(Messages.RequestErrorMessage)
        }
      }
    }

    case GET -> Root / LongVar(productId) / "orders" => {
      logger.info(s"GET /${productId}/orders")

      try {
        val stockDao = new StockDao(database)
        val orders = stockDao.getOrdersByProduct(productId)

        Ok(orders.asJson)
      } catch {
        case e: Exception => {
          logger.error(Messages.RequestErrorMessage, e)

          Responses.systemErrorResponse(Messages.RequestErrorMessage)
        }
      }
    }

    case request @ POST -> Root / "orders" => {
      logger.info(s"POST /orders")

      try {
        val ordersRequest = request.as(jsonOf[List[OrderIn]])

        ordersRequest.flatMap(orders => {
          val timestamp = Instant.now().getEpochSecond()
          val stockDao = new StockDao(database)
          val productDao = new ProductDao(database)
          val confirmedOrders: ListBuffer[OrderOut] = new ListBuffer[OrderOut]()

          for (order <- orders) {
            productDao.getById(order.productId) match {
              case Some(p) => {
                confirmedOrders += stockDao.addOrder(p, order.quantity, timestamp)

                productDao.update(order.productId, p.stock + order.quantity)
              }
            }
          }

          Ok(confirmedOrders.asJson)
        }).handleWith {
          case e: DecodeFailure => {
            logger.error(s"Invalid JSON object: ${request.body}", e)

            Responses.userErrorResponse(Messages.InvalidJsonMessage)
          }
        }
      } catch {
        case NonFatal(e) => {
          logger.error(s"Unexpected error", e)

          Responses.systemErrorResponse(Messages.RequestErrorMessage)
        }
      }
    }
  }
}
