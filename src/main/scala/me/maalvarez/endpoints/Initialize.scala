package me.maalvarez.endpoints

import me.maalvarez.database.CategorySchema.categories
import me.maalvarez.database.ClientSchema.clients
import me.maalvarez.database.OrderSchema.orders
import me.maalvarez.database.ProductSchema.products
import me.maalvarez.database.PurchaseSchema.purchases
import me.maalvarez.database.ReserveSchema.reserves
import journal.Logger
import org.http4s.HttpService
import org.http4s.dsl._
import org.sqlite.SQLiteException
import slick.jdbc.SQLiteProfile.api._
import me.maalvarez.util._

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object Initialize extends AbstractEndpoint {
  private val logger = Logger[this.type]

  val service = HttpService {
    case POST -> Root / "initialize" => {
      logger.info(s"POST /initialize")

      try {
        Await.result({
          database.run(DBIO.seq(
            categories.schema.create,
            clients.schema.create,
            orders.schema.create,
            products.schema.create,
            purchases.schema.create,
            reserves.schema.create))
        }, Duration.Inf)

        NoContent()
      } catch {
        case e: SQLiteException => {
          logger.warn(Messages.InitializationWarningMessage, e)

          Responses.userErrorResponse(Messages.InitializationWarningMessage)
        }
        case e: Exception => {
          logger.error(Messages.InitializationErrorMessage, e)

          Responses.systemErrorResponse(Messages.InitializationErrorMessage)
        }
      }
    }
  }
}
