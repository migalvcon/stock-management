package me.maalvarez.util

import io.circe._
import org.http4s.circe._
import org.http4s.dsl._
import io.circe.literal._
import org.http4s.Status
import org.specs2.matcher.TaskMatchers
import org.specs2.mutable.Specification

class ResponsesSpec extends Specification with TaskMatchers {
  "Responses".br.tab(1)

  "systemErrorResponse" should {
    val response = Responses.systemErrorResponse("systemErrorMessage")

    "return the status code InternalServerError" in {
      response.map(_.status) must returnValue(Status.InternalServerError)
    }

    "return the body as a proper json object" in {
      response.as[Json] must returnValue(json"""{"systemMessage": "systemErrorMessage"}""")
    }
  }

  "userErrorResponse" should {
    val response = Responses.userErrorResponse("userErrorMessage")

    "return the status code InternalServerError" in {
      response.map(_.status) must returnValue(Status.BadRequest)
    }

    "return the body as a proper json object" in {
      response.as[Json] must returnValue(json"""{"userMessage": "userErrorMessage"}""")
    }
  }

  "clientNotFoundErrorResponse" should {
    val response = Responses.clientNotFoundErrorResponse(12345)

    "return the status code InternalServerError" in {
      response.map(_.status) must returnValue(Status.BadRequest)
    }

    "return the body as a proper json object" in {
      response.as[Json] must returnValue(json"""{"userMessage": ${Messages.clientNotFoundMessage(12345)}}""")
    }
  }
}
